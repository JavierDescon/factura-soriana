﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;

namespace FacturasSoriana
{
    class Program
    {
        //static bool entro = false;
        //static string Bitacora;
        //static string server = "LAPTOP-FFMQJASQ";
        //static string databse = "UpSoriana";
        //static string userDb = "sa";
        //static string pass = "Admin123";
        //static string ruta = "C:\\Users\\Descon2\\Desktop\\Proyectos 2019\\FacturasSoriana\\Procesados\\";

        static readonly string rutaPendientes = ConfigurationSettings.AppSettings.Get("rutaPendientes");
        static readonly string rutaProcesados = ConfigurationSettings.AppSettings.Get("rutaProcesados");
        static readonly string rutaProcesadosConError = ConfigurationSettings.AppSettings.Get("rutaProcesadosConError");
        static readonly string rutaBitacoras = ConfigurationSettings.AppSettings.Get("rutaBitacoras");
        static readonly int debugParam = Convert.ToInt32(ConfigurationSettings.AppSettings.Get("debugParam"));
        static string fecha = DateTime.Now.ToString("dd-MM-yyyy--HH;mm;ss");//capturamos la fecha

        static void Main(string[] args)
        {
            try
            {
                int contador=0;
                int archivosCorrectos = 0;
                int archivosErroneos= 0;
                Console.WriteLine("Procesando Archivos desde: "+ rutaPendientes +"...");
                //Obtenemos todos y cada uno de loa archivos dentro del fichero determinado
                DirectoryInfo archivos = new DirectoryInfo(@""+rutaPendientes);
                List<Archivo> listaArchivos = new List<Archivo>();
                foreach (var file in archivos.GetFiles())
                {
                    listaArchivos.Add(new Archivo(file.FullName, file.Name, file.Extension));//Se capturan la ruta,nombre y extension del archivo en una lista de tipo Archivo
                }
               
                WebServiceSoriana.wseDocReciboSoapClient client = new WebServiceSoriana.wseDocReciboSoapClient(); //instanciamos un cliente para hacersolicitudes al WS

                //client.Open();
                using (StreamWriter bitacora = new StreamWriter(@"" +rutaBitacoras + "Bitacora " + fecha + ".txt")) //creamos el nuevo archivo de bitacora (.txt)
                {
                    if (listaArchivos.Count == 0)//en caso de que no halla archivos para procesar
                    {
                        bitacora.WriteLine("No se ha procesado ningun archivo"); //Escribimos en la bitacora
                        Console.WriteLine("\t EL directorio esta vacio");
                    }
                    foreach (var archivo in listaArchivos) //Recorremos todos los Archivos
                    {
                        Mensaje msg = new Mensaje();
                        Console.WriteLine("");
                        if (archivo.extension == ".xml")//verificamos si es un xml
                        {
                            contador++;
                            
                            XmlDocument reader = new XmlDocument();//Creamos un XMlDocument
                            reader.Load(archivo.ruta); //Lo cargamos desde la hubicacion deseada
                            string xml = reader.OuterXml;//Obtenemos el String completo del xml
                            XmlDataDocument response = new XmlDataDocument(); //La respuesta, como sera xml, se capturara dentro de un XmlDocument
                            string mensaje = "";

                            try
                            {
                                response.LoadXml(client.RecibeCFD(xml));//Se envia el string del xml a travez del metodo del WS y guardamos la respuesta en otro XmlDocument
                                

                                msg.estado = response.ChildNodes[1].Attributes[5].InnerText; //capturamos el estado de la respuesta (Reject ó Accepted)

                                msg.mensaje = response.ChildNodes[1].LastChild.ChildNodes[1].InnerText;//Obtenemos el mensaj desde el XML de respuesta

                                msg.errorEnvio = false;
                            }
                            catch (Exception e)
                            {
                                //mensaje = e.Message;
                                msg.errorEnvio = true;
                                msg.estado = "NoEnviado";
                                msg.mensaje = e.Message; //capturamos el mensaje de la excepcion
                                bitacora.WriteLine(archivo.nombre + ": " + msg.ToString()); //Lo escribimos en la biacora
                            }

                            if (msg.estado == "REJECT") //Si el Xml es rechazado
                            {
                                archivosErroneos++;
                                //procesarBitacora(mensaje, 1, 2, 3);
                                Console.WriteLine(archivo.nombre + " (Procesado) " + "Estado:" + msg.ToString());
                                try
                                {
                                    File.Move(rutaPendientes + "" + archivo.nombre, rutaProcesadosConError + "" + archivo.nombre);//Mover a Procesados con error
                                    bitacora.WriteLine(archivo.nombre + ": " + msg.ToString() + " Estado: Movido a: " + rutaProcesadosConError); //Escribimos en la bitacora
                                    Console.Write("Archivo Movido");
                                }
                                catch (Exception e)
                                {
                                    bitacora.WriteLine(archivo.nombre + ": " + msg.ToString() + " Estado: " + e.Message); //Escribimos en la bitacora; Excepcion en caso de que no se pueda mover a procesados con error
                                    Console.Write(e.Message);
                                }
                            }
                            else if ((msg.estado != "REJECT" && msg.estado != "NoEnviado") || msg.estado == "ACCEPTED")//En caso de que sea Aceptado
                            {
                                archivosCorrectos++;
                                Console.WriteLine(archivo.nombre + " (Procesado) " + "Estado:" + msg.ToString());
                                try
                                {
                                    File.Move(rutaPendientes + "" + archivo.nombre, rutaProcesados + "" + archivo.nombre);//Mover a procesados con exito
                                    
                                }
                                catch (Exception e)
                                {
                                    bitacora.WriteLine(archivo.nombre + ": " + msg.ToString() + " Estado: " + e.Message);//Excepcion en caso de que no se pueda mover a procesados con exito
                                    Console.Write(e.Message);
                                }
                                //XmlDocument guardar = new XmlDocument();
                                //guardar.LoadXml(xml);
                                //guardar.Save(ruta + "Addenda" + DateTime.Now.ToString("ddMMyyyy"));
                            }
                        }
                        else //En caso de que no sea un archivo valido (diferente de .xml)
                        {
                            msg.errorEnvio = true;
                            msg.estado = "No enviado";
                            msg.mensaje = "El archivo no contiene una extensión aceptada";
                            try
                            {
                                File.Move(rutaPendientes + "" + archivo.nombre, rutaProcesadosConError + "" + archivo.nombre);//Mover a Procesados con error
                                bitacora.WriteLine(archivo.nombre + ": " + msg.ToString() + " Estado: Movido a: " + rutaProcesadosConError);
                                Console.WriteLine(archivo.nombre + "(Procesado) " + "Estado:" + msg.ToString());
                            }
                            catch (Exception e)
                            {
                                bitacora.WriteLine(archivo.nombre + ": " + msg.ToString() + " Estado: No se pudo mover a: " + rutaProcesadosConError);//En caso de que no se pueda mover a Procesados con error se registra en la Bitacora
                                Console.Write(e.Message);
                            }
                        }
                    }
                    bitacora.Close();//Cerrar la edicion de la bitacora
                }
                client.Close();//cerrar conexion con el Web Service
                Console.WriteLine("");
                Console.WriteLine("Proceso finalizado (Se han procesado " + contador +" archivos, "+ archivosErroneos+" erroneos, "+archivosCorrectos +" correctos)" );
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            if (debugParam == 1)
            {
                Console.ReadKey();

            }
        }

        //Con bitacora a una BD
        //public static void procesarBitacora(string mensaje, int docNum, int docEntry, int tipo) {
        //    try
        //    {
        //        if (entro==false)
        //        {
        //            entro = true;
        //            string FechaBitacora = DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.ToString("HHmmss");
        //            Bitacora = "Bita" + FechaBitacora;
        //        }
        //        SqlConnection miConexion= new SqlConnection("server='"+server+"';database='"+databse+"';uid='"+userDb+"';password='"+pass+"'");
        //        DateTime uno1;
        //        string fecha1 = DateTime.Today.ToShortDateString().ToString();
        //        //if (IdiomaSQL  == 0)
        //        //{
        //        //    uno1 = Convert.ToDateTime(fecha1.ToString());
        //        //    fecha1 = uno1.ToString("MM/dd/yyyy");
        //        //}
        //        string hora = DateTime.Now.ToString("HH:mm:ss");
        //        string consulta = "Insertar";
        //        SqlCommand ejecuta = new SqlCommand(consulta, miConexion);
        //        ejecuta.CommandType = System.Data.CommandType.StoredProcedure;
        //        ejecuta.Parameters.Add(new SqlParameter("@fecha", fecha1));
        //        ejecuta.Parameters.Add(new SqlParameter("@Hora", hora));
        //        ejecuta.Parameters.Add(new SqlParameter("@DocNum", docNum));
        //        ejecuta.Parameters.Add(new SqlParameter("@DocEntry", docEntry));
        //        ejecuta.Parameters.Add(new SqlParameter("@Mensaje", mensaje));
        //        ejecuta.Parameters.Add(new SqlParameter("@tipo", tipo));
        //        ejecuta.Parameters.Add(new SqlParameter("@nombre", Bitacora));
        //        miConexion.Open();
        //        ejecuta.ExecuteNonQuery();
        //        miConexion.Close();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Error al registrar en bitacora: "+ e.Message);
        //    }
        //}
    }
}
