﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacturasSoriana
{
    class Archivo
    {
        public string ruta;
        public string nombre;
        public string extension;
        public Archivo(string ruta,string nombre,string extension) {
            this.ruta = ruta;
            this.nombre = nombre;
            this.extension = extension;
        }
    }
}
