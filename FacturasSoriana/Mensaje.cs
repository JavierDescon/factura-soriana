﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacturasSoriana
{
    class Mensaje
    {
        public bool errorEnvio;
        public string estado;
        public string mensaje;

        public override string ToString()
        {
            return "[ErrorEnvio: " + errorEnvio + ", Estado: " + estado + ", Mensaje: " + mensaje+"]";
        }
    }
}
